/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author nmessias
 */
public class Elipse {
    private double area;
    private double perimetro;
    private double r, s;
    
    public Elipse(double r, double s) {
        this.r = r;
        this.s = s;
    }

    public double getArea() {
        return Math.PI * this.r * this.s;
    }

    public double getPerimetro() {
        return Math.PI * (3 * (r + s) - Math.sqrt((3 * r + s) * (r + 3 * s)));
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public double getS() {
        return s;
    }

    public void setS(double s) {
        this.s = s;
    }
}
